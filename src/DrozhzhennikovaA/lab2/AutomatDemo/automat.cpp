#include "automat.h"
#include<mainwindow.h>
#include<QTimer>
#include<QMessageBox>
#include<QDebug>

Automat::Automat()
{
    money=0;
    price=0;
    status=WAIT;
    QString str=0;
}

QString Automat::printState()
{
    if (status == WAIT)
        return QString("HELLO!please, deposit money to an account!");
    if (status == CHECK)
        return QString("Please wait!");
    if (status == COOK)
        return QString("Please wait, we are cooking for you!\n");
    if(status==PROBLEM)
        return QString("Not enough money on your account, please insert more or interrupt the cooking\n");
    if (status == CHANGE)
        return QString("Here is your change!\n");
    if (status == READY)
        return QString("Your coffee is ready\n");
    if (status == FINISH)
        return QString("Finish!");
}

void Automat::choice()//выбор напитка пользователем;
{     
    Automat MyAutomat;
    MyAutomat.status=CHECK;
    qDebug() << "выбор";
}
int Automat::check(int price)//проверка наличия необходимой суммы;
{
    Automat MyAutomat;
    if(MyAutomat.status==CHECK)
    {
    int account=MyAutomat.getPrice();

        if(account==price)
            MyAutomat.status=COOK;
            MyAutomat.printState();
            MyAutomat.cook();
            return 0;
        if(account>price)
        {
            MyAutomat.status=CHANGE;
            int difference=account-price;
            if((MyAutomat.change(difference))==0)
            {
                MyAutomat.status=COOK;
                MyAutomat.cook();
            }
            return difference;
        }
        if(account<MyAutomat.price)
        {
            status=PROBLEM;//сообщение об ошибке и два варианта
            MyAutomat.printState();
            status = WAIT;
            return -1;
        }
    }
    return 0;
}
int Automat::change(int a)
{
    Automat MyAutomat;
    if (MyAutomat.status==CHANGE)
    {
    int account=MyAutomat.getPrice();
    money=account-a;
    }
     qDebug() <<money;
    return 0;
}
void Automat::cancel() //отмена сеанса обслуживания пользователем;
{
    Automat MyAutomat;
    MyAutomat.change(money);
    price=0;
}

void Automat::cook() //имитация процесса приготовления напитка;
{
    Automat MyAutomat;
    MyAutomat.status=COOK;
     qDebug() <<"готовим";
}
void Automat::finish() //завершение обслуживания пользователя.
{
    Automat MyAutomat;
    MyAutomat.status=FINISH;
}


