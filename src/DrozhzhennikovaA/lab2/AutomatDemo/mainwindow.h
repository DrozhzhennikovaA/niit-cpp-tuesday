#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include"automat.h"
#include <QMap>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    std::map <QString, int> menu;
    std::map <QString, int>::iterator it;
    QTimer *qt;

private slots:
    void on_pbBlackC_clicked();

    void on_pbCap_clicked();

    void on_pbEspr_clicked();

    void on_pbTee_clicked();

    void on_pbWat_clicked();

    void on_pb_1_clicked();

    void on_pb_2_clicked();

    void on_pb_5_clicked();

    void on_pb_10_clicked();

    void on_pb_50_clicked();

    void on_pb_100_clicked();

    void on_pbMenu_clicked();

    void start();

    void stop();

    void timeout();

    void on_pbCook_clicked();

    void on_pbCansel_clicked();

private:
    Ui::MainWindow *ui;
    Automat MyAutomat;
    int tval=0;
};

#endif // MAINWINDOW_H
