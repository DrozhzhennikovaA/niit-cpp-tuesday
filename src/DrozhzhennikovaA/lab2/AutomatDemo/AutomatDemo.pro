#-------------------------------------------------
#
# Project created by QtCreator 2016-04-08T15:33:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AutomatDemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    automat.cpp

HEADERS  += mainwindow.h \
    automat.h

FORMS    += mainwindow.ui
