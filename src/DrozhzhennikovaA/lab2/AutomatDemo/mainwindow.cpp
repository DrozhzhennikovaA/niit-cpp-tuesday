#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "automat.h"
#include<QFile>
#include<QMap>
#include<QMessageBox>
#include <QDebug>
#include<QXmlStreamReader>
#include<QXmlStreamAttributes>
#include <QTextEdit>
#include<QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qt=new QTimer;
    ui->progressBar->setValue(0);
    connect(ui->pbCook, SIGNAL(clicked()), this, SLOT(start()));    // for progress Bar
    connect(this->qt, SIGNAL(timeout()), this, SLOT(timeout()));    // for progress Bar

    QFile* pfile = new QFile ("C://Users/Bob/Desktop/QtProjects/AutomatDemo/menu.xml");
    QString title;
    int cost;

    if(!pfile->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "файл не читается";
        return;
    }
    QXmlStreamReader xml(pfile);
    while (!xml.atEnd() && !xml.hasError())

    {
        QXmlStreamReader::TokenType token = xml.readNext();
        if(token==QXmlStreamReader::StartDocument)
        {
            continue;
        }
        if(token==QXmlStreamReader::StartElement)
        {
            QXmlStreamAttributes attributes=xml.attributes();
            if(attributes.hasAttribute("title"))
            {
                title=attributes.value("title").toString();
                qDebug() << title;
            }
            if(attributes.hasAttribute("cost"))
            {
                cost=attributes.value("cost").toInt();
                qDebug() << cost;
            }
            menu.insert(std::make_pair(title,cost));
        }
     }
   }

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::timeout()
{
    if(tval<=100)
    {
        ui->leMessage->setText(MyAutomat.printState());
        ui->progressBar->setValue(tval++);
    }
    else
    {
        ui->leMessage->setText(MyAutomat.printState());
        qt->stop();

    }
}
void MainWindow::start()
{
  qt->setInterval(500);
  qt->start();
}
void MainWindow::stop()
{
  qt->stop();
}


void MainWindow::on_pbBlackC_clicked()
{
    MyAutomat.str="BlackCoffe";
    MyAutomat.choice();
    ui->leMessage->setText(MyAutomat.printState());
}

void MainWindow::on_pbCap_clicked()
{
    MyAutomat.str="Cappucino";
    MyAutomat.choice();
    ui->leMessage->setText(MyAutomat.printState());
}

void MainWindow::on_pbEspr_clicked()
{
    MyAutomat.str="Espresso";
    MyAutomat.choice();
    ui->leMessage->setText(MyAutomat.printState());
}

void MainWindow::on_pbTee_clicked()
{
    MyAutomat.str="Tee";
    MyAutomat.choice();
    ui->leMessage->setText(MyAutomat.printState());
}

void MainWindow::on_pbWat_clicked()
{
    MyAutomat.str="Water";
    MyAutomat.choice();
    ui->leMessage->setText(MyAutomat.printState());
}



void MainWindow::on_pb_1_clicked()
{
    MyAutomat.summa(1);
}

void MainWindow::on_pb_2_clicked()
{
    MyAutomat.summa(2);
}

void MainWindow::on_pb_5_clicked()
{
    MyAutomat.summa(5);
}

void MainWindow::on_pb_10_clicked()
{
    MyAutomat.summa(10);
}

void MainWindow::on_pb_50_clicked()
{
    MyAutomat.summa(50);
}

void MainWindow::on_pb_100_clicked()
{
    MyAutomat.summa(100);
}

void MainWindow::on_pbMenu_clicked()
{
    qDebug() << "печать";
    menu.erase(menu.begin());
    for (it=menu.begin(); it!=menu.end(); it++)
    ui->textEdit->append(it->first+':'+QString::number(it->second));
}

void MainWindow::on_pbCook_clicked()
{
    QString &strn=MyAutomat.str;
    qDebug() << strn;
    auto price=(menu.find(strn)->second);
    qDebug() << price;
    if (price!=0)
    {
       int difference=MyAutomat.check(price);
       if(difference>0)
       {
           ui->leMessage->setText("Your change is "+ ' '+ QString::number(difference));
       }
    }
}


void MainWindow::on_pbCansel_clicked()
{
    MyAutomat.cancel();
    ui->leMessage->setText("You've interrupt the cooking!\n");
}
