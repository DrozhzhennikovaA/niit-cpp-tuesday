#ifndef AUTOMAT_H
#define AUTOMAT_H
#include <QMessageBox>
#include <QPushButton>
#include <QFile>
#include <QXmlStreamReader>
#include <QIODevice>
#include <QTimer>
#include <QString>
enum State {OFF,WAIT,CHECK,COOK,CHANGE,PROBLEM,READY,FINISH};

class Automat
{
private:
    int money;
    int price;
public:
    Automat(); //конструктор
    void summa(int n){money+=n;}//занесение денег на счёт пользователем;     OK
    int getPrice(){return money;}//получить значение счета;                 OK
    State get_status() const { return status; }//отображение текущего состояния для пользователя;OK
    void choice(); //выбор напитка пользователем;                       OK
    int check(int price); //проверка наличия необходимой суммы;             OK
    int change(int a);//сдача                                               OK
    void cancel(); //отмена сеанса обслуживания пользователем;               OK
    void cook(); //имитация процесса приготовления напитка    таймер         OK
    void finish(); //завершение обслуживания пользователя.                   OK
    QString printState();// проверка статуса и переключение на другую функцию OK
    State status;
    QString str;// OK
};

#endif // AUTOMAT_H





