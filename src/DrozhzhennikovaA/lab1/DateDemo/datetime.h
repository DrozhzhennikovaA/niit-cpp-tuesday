#ifndef DATETIME_H
#define DATETIME_H
#include<ctime>
#include <QMainWindow>



class DateTime 
{


public:
     DateTime();
     DateTime(int,int,int);
     DateTime(const DateTime& str);
     DateTime& operator= (const DateTime&);
     tm *get_future(int);
     tm *get_past(int);
     time_t setDate();
     time_t setDate(int ,int ,int );
     time_t get_cur_Date(){return time_count;}
     time_t get_print_Date() {return time_input;}

     void get_difference(int a);
     time_t time_input;
     time_t time_count;

private:



};

#endif // DATETIME_H
