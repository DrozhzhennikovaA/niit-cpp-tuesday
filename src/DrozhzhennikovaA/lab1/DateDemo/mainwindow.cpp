#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "datetime.h"
#include <ctime>
#include<time.h>
#include <QLineEdit>
#include<QLabel>
#include<QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    DateTime date;
    DateTime date_count;

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbToday_clicked()
{
    date_count.setDate();
    time_t TIME=date_count.get_cur_Date();
    ui->label_Today->setText(ctime(&TIME));
}

DateTime::DateTime()
{
    time_count=time(&time_count);
}
DateTime::DateTime(int day,int month, int year)
{
    tm  t_struct;
    t_struct.tm_sec = 0;
    t_struct.tm_min = 0;
    t_struct.tm_hour = 0;
    t_struct.tm_mday=day;
    t_struct.tm_mon=month-1;
    t_struct.tm_year=year-1900;
    t_struct.tm_wday = 0;
    t_struct.tm_yday = 365;
    t_struct.tm_isdst = 0;
    time_input=mktime(&t_struct);
}


DateTime::DateTime(const DateTime &str)
{
    time_count = str.time_count;
    time_input= str.time_input;
}

DateTime& DateTime::operator= (const DateTime& N)
{
    time_count = N.time_count;
    time_input=N.time_input;
    return *this;
}

time_t DateTime::setDate()
{
    time_count=time(NULL);
    return 0;
}
time_t DateTime::setDate(int day,int month,int year)
{
    tm  t_struct;
    t_struct.tm_sec = 0;
    t_struct.tm_min = 0;
    t_struct.tm_hour = 0;
    t_struct.tm_mday=day;
    t_struct.tm_mon=month-1;
    t_struct.tm_year=year-1900;
    t_struct.tm_wday = 0;
    t_struct.tm_yday = 365;
    t_struct.tm_isdst = 0;
    time_input=mktime(&t_struct);
   return time_input;
}

/*int DateTime::get_Day()
{
    t_struct->tm_mday;
}
int DateTime::get_Month()
{
    t_struct->tm_mon+1;
}
int DateTime::get_Year()
{
    t_struct->tm_year+1900;
}
*/
tm* DateTime::get_future(int a)
{
    tm  *t_struct;
    t_struct=localtime(&time_input);
    t_struct->tm_mday=t_struct->tm_mday+a;
    int diff=0;
       if(t_struct->tm_mday>30)
       {
           diff=a;
           if(t_struct->tm_mon<12&&t_struct->tm_mon>0)
           {
               t_struct->tm_mday=diff;
               t_struct->tm_mon+=1;
           }
           if(t_struct->tm_mon>12)
               t_struct->tm_year+=1;
       }
    return t_struct;
}
tm *DateTime::get_past(int a)
{
    tm  *t_struct;
    t_struct=localtime(&time_input);
       t_struct->tm_mday=t_struct->tm_mday-a;
       int diff=0;
          if(t_struct->tm_mday<0)
          {
              diff=30-a;
              if(t_struct->tm_mon<12&&t_struct->tm_mon>0)
              {
                  t_struct->tm_mday=diff;
                  t_struct->tm_mon-=1;
              }
              if(t_struct->tm_mon<0)
                  t_struct->tm_year-=1;
          }
    return t_struct;
}

/*void DateTime::get_difference(int a)
{
    int time=((time_count-time_input)/86400);
    if (time_count>time_input)
            return time;
        if(time_count==time_input)
            return 0;
        else
            return (time*(-1));
    t_struct.tm_mday+=a;
       if(t_struct.tm_mday>30)
       {
           if(t_struct.tm_mon<12&&t_struct.tm_mon>0)
           {
               t_struct.tm_mday=1;
               t_struct.tm_mon+=1;
           }
           if(t_struct.tm_mon>12)
               t_struct.tm_year+=1;
       }
}
*/

void MainWindow::on_pbPast_clicked()
{
    QString a=ui->leDiffDays->text();
    int b=a.toInt();
   //date.get_difference(b);
    time_t time_input=date.time_input;
    ui->teDifference->setText(ctime(&time_input));
    ui->leDay->setText(QString::number(date.get_past(b)->tm_mday));
    ui->leMonth->setText(QString::number(date.get_past(b)->tm_mon+1));
    ui->leYear->setText(QString::number(date.get_past(b)->tm_year+1900));
    //ui->teDifference->setText(QString::number(N));
}

void MainWindow::on_pbFuture_clicked()
{
    QString a=ui->leDiffDays->text();
    int b=a.toInt();
    time_t time_input=date.get_print_Date();
    ui->teDifference->setText(ctime(&time_input));
    //date.get_difference(b);
    ui->leDay->setText(QString::number(date.get_future(b)->tm_mday));
    ui->leMonth->setText(QString::number(date.get_future(b)->tm_mon+1));
    ui->leYear->setText(QString::number(date.get_future(b)->tm_year+1900));
   // ui->teDifference->setText(QString::number(N));*/
}


void MainWindow::on_pbSetDate_clicked()
{
    QString d=ui->leDay->text();
    QString m=ui->leMonth->text();
    QString y=ui->leYear->text();
    int day = d.toInt();
    int month = m.toInt();
    int year = y.toInt();
    time_t time_input=date.setDate(day,month,year);
    ui->teDifference->setText(ctime(&time_input));//проверка считывается ли ок!
}

void MainWindow::on_pbUpDate_clicked()
{
    QString d=ui->leDay->text();
    QString m=ui->leMonth->text();
    QString y=ui->leYear->text();
    date.setDate(d.toInt(),m.toInt(),y.toInt());
    time_t time_count=date.get_cur_Date();//count
    time_t time_input=date.get_print_Date();
    ui->leDate_input->setText(ctime(&time_input));
    ui->leDate_count->setText(ctime(&time_count));

}
