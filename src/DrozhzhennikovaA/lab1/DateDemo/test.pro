#-------------------------------------------------
#
# Project created by QtCreator 2016-04-03T18:05:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    datetime.h

FORMS    += mainwindow.ui
