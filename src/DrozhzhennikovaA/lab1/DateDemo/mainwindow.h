#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include"datetime.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();


private slots:
    void on_pbToday_clicked();

    void on_pbPast_clicked();

    void on_pbFuture_clicked();

    void on_pbSetDate_clicked();

    void on_pbUpDate_clicked();

private:
    Ui::MainWindow *ui;
    DateTime date,date_count;

};



#endif // MAINWINDOW_H
