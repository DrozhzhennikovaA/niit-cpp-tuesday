#-------------------------------------------------
#
# Project created by QtCreator 2016-04-03T20:09:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Circle
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    circle_class.h

FORMS    += mainwindow.ui
