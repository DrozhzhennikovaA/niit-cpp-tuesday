#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "circle_class.h"
#include <cmath>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

Circle_class::Circle_class()
{

}



void Circle_class::setRadius(double r)
{
    radius = r;
    ference = 2.0 * 3.14159 * radius;
    square = 3.14159 * radius * radius;
}

void Circle_class::setFerence(double f)
{
    ference=f;
    radius=f/(2*3.14159);
    square=3.14159*radius*radius;
}

void Circle_class::setArea(double a)
{
    square =a;
    radius=sqrt(square/3.14159);
    ference=2*3.14159*radius;
}
int Circle_class::getRadius()
{return radius;}
int Circle_class::getFerence()
{return ference;}
int Circle_class::getArea()
{return square;}


void MainWindow::on_pbRadius_clicked()
{
    Circle_class MyCircle;
    QString r = ui->leRadius->text();
    double radius=r.toDouble();
    MyCircle.setRadius(radius);
    double f=MyCircle.getFerence();
    ui->leFerence->setText(QString::number(f));
    double a=MyCircle.getArea();
    ui->leArea->setText(QString::number(a));
}

void MainWindow::on_pbFerence_clicked()
{
    Circle_class MyCircle;
    QString f = ui->leFerence->text();
    double ference=f.toDouble();
    MyCircle.setFerence(ference);
    double r=MyCircle.getRadius();
    ui->leRadius->setText(QString::number(r));
    double a=MyCircle.getArea();
    ui->leArea->setText(QString::number(a));
}

void MainWindow::on_pbArea_clicked()
{
    Circle_class MyCircle;
    QString a = ui->leArea->text();
    double area=a.toDouble();
    MyCircle.setArea(area);
    double r=MyCircle.getRadius();
    ui->leRadius->setText(QString::number(r));
    double f=MyCircle.getFerence();
    ui->leFerence->setText(QString::number(f));
}
