#ifndef CIRCLE_CLASS_H
#define CIRCLE_CLASS_H


class Circle_class
{
public:
    Circle_class();
    void setRadius(double r);
    void setFerence(double f);
    void setArea(double a);
    int getRadius();
    int getFerence();
    int getArea();

private:
    double radius,ference,square;
};


#endif // CIRCLE_CLASS_H
